# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

The MATLAB sourcecode contains features and OMIM diseases as MAT files. The dataset is used fully or partly in the following publications:

1. Inductive matrix completion for predicting gene-disease associations (pdf, software)
N. Natarajan, I. Dhillon.
Bioinformatics 30(12), pp. i60-i68, June 2014.
2. Prediction and Validation of Gene-Disease Associations using Methods Inspired by Social Network Analyses (pdf)
U. Singh-Blom, N. Natarajan, A. Tewari, J. Woods, I. Dhillon, E. Marcotte.
PLoS ONE 8(5), May 2013.
(e58977)

For more details, visit: http://bigdata.ices.utexas.edu/project/gene-disease/
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact